#!/usr/bin/env bash

# Get latest code
sudo apt install -y git
cd
git clone https://bitbucket.org/procesosiq/agroiq-trutest-rpi.git
cd agroiq-trutest-rpi/
git pull

# Install and run reverse shell
# mkdir -p ~/.config/systemd/user
# cp ~/agroiq-trutest-rpi/systemd/rssh.service ~/.config/systemd/user/
sudo cp ~/agroiq-trutest-rpi/systemd/rssh.service /etc/systemd/system/
read -p 'SSH port: ' SSHPORT
sudo sed -i "s/\$SSHPORT/$SSHPORT/g" /etc/systemd/system/rssh.service
sudo systemctl enable rssh.service

# Generate key
ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ''
echo 'Register public key'
cat ~/.ssh/id_rsa.pub

# Change default insecure password
sudo passwd pi

# Change hostname
read -p 'New hostname: ' HOSTNAME
sudo sed -i "s/raspberrypi/$HOSTNAME/g" /etc/hostname
sudo sed -E -i "s/127\.0\.1\.1\s+raspberrypi/127.0.1.1               $HOSTNAME/g" /etc/hosts
sudo sync
sudo reboot