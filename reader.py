#!/usr/bin/env python3

import time
import sqlite3

from datetime import datetime
from lxml import etree, objectify

db_path = 'data.db'


def init_db():

    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("""
        CREATE TABLE IF NOT EXISTS trutest (
            key TEXT NOT NULL UNIQUE,
            session_id INTEGER NOT NULL,
            file_name TEXT NOT NULL,
            file_data TEXT NOT NULL,
            created_at INTEGER NOT NULL DEFAULT (strftime('%s', 'now')),
            updated_at INTEGER NOT NULL DEFAULT (strftime('%s', 'now'))
        )""")
    conn.commit()
    c.execute("""
        CREATE TRIGGER IF NOT EXISTS trutest_update AFTER UPDATE ON trutest
        BEGIN
            UPDATE trutest SET updated_at = strftime('%s', 'now') WHERE key = NEW.key;
        END;
    """)
    conn.commit()
    conn.close()


def save_to_db(data):

    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    sql = 'INSERT INTO trutest %s VALUES %s' % (
            tuple(data),tuple([data[i] for i in data]))
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    conn.close()


def get_last_data(key):
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    sql = 'SELECT file_data FROM trutest WHERE "key" = "%s"' % (key)
    cursor.execute(sql)
    q = cursor.fetchone()
    cursor.close()
    conn.close()
    if q:
        return q[0]
    else:
        return None


def update_data(key, data):
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    sql = """UPDATE trutest SET file_data = '%s' WHERE "key" = '%s'""" % (data, key) 
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    conn.close()


def get_sessions(ip):

    url = 'http://%s/sessions' % ip
    xml = etree.parse(url)
    root = objectify.fromstring(etree.tostring(xml))

    sessions = []

    for session in root.sessions.getchildren():

        d = (session.session_id,
             session.name,
             session.animals.attrib['count'])

        sessions.append(d)

    sessions.sort(reverse=True)

    return sessions


def get_animals(ip, sid):

    url = 'http://%s/sessions/%s' % (ip, sid)
    xml = etree.parse(url)
    root = objectify.fromstring(etree.tostring(xml))
    data = []

    for session in root.sessions.getchildren():
        header = [session.session_id, session.name, 
                  session.animals.attrib['count']]

    for animal in root.sessions.session.animals.getchildren():
        data.append(etree.tostring(animal))

    return (header, data)


if __name__ == '__main__':

    import sys
    
    options = ('-l','-s','-h')

    HELP = '''USE %s ip [%s]

             OPTIONS:
             -l     show sessions table
             -s ID  show data from given session id
             -h     help\n''' % (sys.argv[0], ' | '.join(options))

    if len(sys.argv) < 2:
        print(HELP)
        exit(1)

    if len(sys.argv) >= 2:
        ip = sys.argv[1]

        if ip not in options:
            init_db()

            if len(sys.argv) == 2:

                prefix = '{http://www.ads.org/AnimalDataSchema}'
                while True:

                    try:

                        sid = get_sessions(ip)[0][0]
                        animals = get_animals(ip, sid)
                        sid, name, count = animals[0]
                        
                        for animal in animals[1]:
                            file_data = animal.decode('ascii','ignore')

                            animal_elment = etree.fromstring(file_data)
                            weight = ''
                            dt = ''

                            for elem in animal_elment.iter('{0}weight'.format(prefix)):
                                weight = elem.text

                            for elem in animal_elment.iter('{0}datetime'.format(prefix)):
                                dt = elem.text

                            key = '%s-%s-%s' % (sid, weight, dt)

                            data = dict(
                                key=key,
                                session_id=sid,
                                file_name=name,
                                file_data=file_data,
                            )
                            
                            last_data = get_last_data(key)
                            if last_data is None:
                                save_to_db(data)
                            elif file_data != last_data:
                                update_data(key, file_data)

                            data.update(count=len(animals[1]))
                            data.pop('file_data')
                            data.pop('key')

                            today = datetime.strftime(datetime.today(), 
                                    '%d-%m-%Y %H:%M:%S')

                            print('%s [%s]' % (data,today))

                    except Exception as e:
                        print('Exception:', e)
                        time.sleep(30)
                        continue

            if len(sys.argv) >= 3:

                from tabulate import tabulate
                option = sys.argv[2]

                if option not in options:
                    print('invalid option, use -h')
                    print(HELP)
                    exit(1)

                if option == '-h':
                    print('HELP:')
                    print(HELP)
                    exit(1)

                if option == '-l':

                    headers = ['SESSION ID', 'NAME FILE', 'COUNT']
                    sessions = get_sessions(ip)
                    c = len(sessions)

                    print(tabulate(sessions, headers, tablefmt='fancy_grid'))
                    print('%s sessions' % c)

                if option == '-s':

                    if len(sys.argv) != 4:
                        print('Set session ID')
                        print(HELP)
                        exit(1)

                    sid = sys.argv[3]
                    animals = get_animals(ip, sid)
                    sid, name, count = animals[0]
                    
                    header = ['SESSION ID', 'NAME FILE', 'COUNT', 'TOTAL']
                    table = [(sid, name, count, len(animals[1])),]

                    for animal in animals[1]:
                        data = dict(session_id=sid,
                                    file_name=name,
                                    file_data=animal.decode('ascii','ignore'))
                        try:
                            save_to_db(data)
                        except sqlite3.IntegrityError:
                            continue

                    print(tabulate(table, header, tablefmt='fancy_grid'))

        else:
            print('Use ip as first argv')
            print(HELP)
            exit(1)
