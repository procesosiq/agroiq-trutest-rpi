#!/bin/bash
SESSION=$USER

tmux -2 new-session -d -s $SESSION

# Setup a window for reader
tmux rename-window -t $SESSION:0 'reader'
tmux split-window -v

tmux select-pane -t 0
tmux send-keys "/home/pi/reader.py 192.168.17.41" C-m

tmux select-pane -t 1
tmux send-keys "/home/pi/reader.py 192.168.17.42" C-m

# Setup a window for sync
tmux new-window -t $SESSION:1 -n 'sync'

tmux select-pane -t 0
tmux send-keys "/home/pi/sync.py" C-m

# Default Selected Window
tmux select-window -t 0
