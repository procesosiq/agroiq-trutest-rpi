#!/usr/bin/env python3

import time
import socket
import sqlite3
import pymysql.cursors

from lxml import objectify
from datetime import datetime, timedelta


# CONFIG VARS

local_db = 'data.db'
remote_db = 'agroaudit_sumifru'

local_table = 'trutest'
remote_table = 'balanza_trutest_xr5000'


def piq_db_conn(remote_db):

    conn = pymysql.connect(db=remote_db,
                        user='procesosiq',
                        password='u[V(fTIUbcVb',
                        host='procesos-iq.com',
                        port=3306,
                        read_timeout=60,
                        connect_timeout=60)
    
    return conn


def piq_lastsec(piq_conn, remote_table, rpi_name):

    sql = '''SELECT MAX(fecha_rpi) FROM %s
             WHERE nombre_rpi = "%s"''' % (remote_table, rpi_name)

    with piq_conn.cursor() as cursor:

        cursor.execute(sql)

        q = cursor.fetchone()

        if q[0] is None:
            lastsec = 0
        else:
            lastsec = q[0]

        return lastsec
            

def fetch_data(local_table, lastsec):
    last_row_time = lastsec.timestamp()
    max_time = (datetime.now() - timedelta(minutes = 2)).timestamp() 
    conn = sqlite3.connect(local_db)
    cursor = conn.cursor()
    sql = '''SELECT rowid, * FROM %s
             WHERE  updated_at > %s AND updated_at < %s''' % (local_table, last_row_time, max_time) 
    cursor.execute(sql)
    data = cursor.fetchall()
    conn.close()
    
    return data


def keys(key):

    return {'mano': 'manos',
            'idfincas': 'idfinca'}.get(key, key)


def parse_data(row, rpi_name):
    row_id, key, session_id, file_name, file_data, _, updated_ts = row

    fecha = datetime.fromtimestamp(updated_ts).strftime('%Y-%m-%d %H:%M:%S')

    data = dict(
                id_local=key,
                secuencia=row_id,
                numero_sesion=session_id,
                nombre_archivo=file_name,
                nombre_rpi=rpi_name,
                fecha_rpi=fecha)

    root = objectify.fromstring(file_data)

    for i in root.getchildren():
        s = '{http://www.ads.org/AnimalDataSchema}'
        key, value = i.tag.replace(s,''), i.text
        if key != 'customSessionData':
            t = [(key, value)]
        data.update(dict(t))

    try:
        for i in root.customSessionData:
            key, value = str(i.name).lower(), i.value
            key = keys(key)
            t = [(key.replace(' ', '_'), value)]
            data.update(dict(t))
    except:
        pass

    return data


def save_data(piq_conn, remote_table, data):

        try:
            idfinca = data['idfinca']
            idf = str(idfinca).lower().replace(' ', '_')
            data.update(idfinca=idf)
        except:
            pass

        cursor = piq_conn.cursor()
        
        # Verificacion de existencia
        sql = """
            SELECT id FROM %s WHERE id_local = '%s' AND nombre_rpi = '%s' 
        """ % (remote_table, data['id_local'], data['nombre_rpi'])
        cursor.execute(sql)
        q = cursor.fetchone()
        id = q[0] if q else None
        
        if id is None:
            columnas = ','.join(data)
            sql = 'INSERT INTO %s (%s) VALUES %s' % (
                    remote_table, columnas,
                    tuple([data[i] for i in data])
                )
        else:
            nueva_data = ', '.join(list(map(lambda x: "%s = '%s'" % (x[0], x[1]), data.items())))
            sql = 'UPDATE %s SET %s WHERE id = %s' % (
                    remote_table, nueva_data, id
                )
        
        cursor.execute(sql)
        piq_conn.commit()

        print('[%s] Synced' % data['secuencia'])


if __name__ == '__main__':

    rpi_name = socket.gethostname()

    while True:

        l = []
        piq_conn = None

        try:

            piq_conn = piq_db_conn(remote_db)
            lastsec = piq_lastsec(piq_conn, remote_table, rpi_name)

            for row in fetch_data(local_table, lastsec):

                data = parse_data(row, rpi_name)
                save_data(piq_conn, remote_table, data)
                l.append(row[0])

            else:

                rows = len(l)

                if rows == 0:
                    print('No more data lastsec [%s] conected to [%s]' 
                            % (lastsec, piq_conn.host))
                    time.sleep(30)

                if rows > 0:
                    print('[%s] records synced' % rows) 

        except Exception as e:
            print('Exception:', e)
            time.sleep(30)
            continue

        finally:
            if piq_conn:
                piq_conn.close()
